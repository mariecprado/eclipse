package functionalTests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
import auxClasses.WindowHandler;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

//import org.openqa.selenium.support.ui.Select;
/*
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class SeleniumTest extends Browsers{
	private WebElement element;

	@Test
	public void f() {
		try {
			
			driver.get("http://compras135.ufm.edu/");
			//driver.findElement(By.id("lst-ib")).sendKeys("xik.gt\n");
			
			ReadUrlFile.Wait(2000);
			driver.findElement(By.cssSelector("button")).click();
			WebElement email = driver.findElement(By.name("identifier"));
			Actions builder = new Actions(driver);
	        Actions seriesOfActions = builder.moveToElement(email).click().sendKeys(email, "xik");
	        seriesOfActions.perform();
	        driver.findElement(By.id("identifierNext")).click();
	        ReadUrlFile.Wait(3000);
	        WebElement passw = driver.findElement(By.cssSelector("input[class='whsOnd zHQkBf']"));
	        Actions builderp = new Actions(driver);
	        Actions seriesOfActionsp = builderp.moveToElement(passw).click().sendKeys(passw, "xik$2015");
	        seriesOfActionsp.perform();
	        driver.findElement(By.id("passwordNext")).click();
	        ReadUrlFile.Wait(5000);
	        driver.findElement(By.cssSelector("img[src='themes/ufm/images/logo-ufm.png']")).click();
	        driver.get("http://compras135.ufm.edu/index.php");
	        Actions actions = new Actions(driver); 
	        WebElement menuHoverLink = driver.findElement(By.cssSelector("td[menu='menu_2']"));
	        Actions seriesOfActionsmH = actions.moveToElement(menuHoverLink).click();
	        seriesOfActionsmH.perform();
	        actions.moveToElement(menuHoverLink).perform();
	        driver.findElement(By.linkText("Administración académica")).click();
	        ReadUrlFile.Wait(2000);
	        Actions actionsCA = new Actions(driver); 
	        WebElement menuHoverLinkCA = driver.findElement(By.id("tdItemMenu_4"));
	        Actions seriesOfActionsCA = actionsCA.moveToElement(menuHoverLinkCA).click();
	        seriesOfActionsCA.perform();
	        actionsCA.moveToElement(menuHoverLinkCA).perform();
	        driver.findElement(By.linkText("Administración de LOGOS")).click();
	        driver.findElement(By.id("buttClassListadoLogosNuevaSolicitud")).click();
	        WindowHandler.next(driver);
	        ReadUrlFile.waitForLoad(driver);
	        Actions builderNF = new Actions(driver);
	        WebElement nameNF= driver.findElement(By.id("txtNombre"));
	        Actions seriesOfActionsNF = builderNF.moveToElement(nameNF).click().sendKeys(nameNF, "Karl Heinz Chávez Asturias");
	        seriesOfActionsNF.perform();
	        Actions builderCN = new Actions(driver);
	        WebElement nameCN = driver.findElement(By.id("txtCarne"));
	        Actions seriesOfActionsCN = builderCN.moveToElement(nameCN).click().sendKeys(nameCN, "20100057");
	        seriesOfActionsCN.perform();
	      
	        Select dropdownrol = new Select(driver.findElement(By.id("sltLogoOrigen")));
	        dropdownrol.selectByVisibleText("Catedrático");
	        
	        driver.findElement(By.tagName("button")).click();
	        element = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("label[class='ui-corner-all ui-state-active']")));
	  
	        Actions builderDesc = new Actions(driver);
	        WebElement nameDesc = driver.findElement(By.id("txtDescripcion"));
	        Actions seriesOfActionsDesc = builderDesc.moveToElement(nameDesc).click().sendKeys(nameDesc, "Nuevo Logo");
	        seriesOfActionsDesc.perform();
	        Actions builderCant= new Actions(driver);
	        WebElement nameCant = driver.findElement(By.name("txtLogoAcredita"));
	        Actions seriesOfActionsCant = builderCant.moveToElement(nameCant).click().sendKeys(nameCant, "5");
	        seriesOfActionsCant.perform();
	        Actions builderCupo= new Actions(driver);
	        WebElement nameCupo = driver.findElement(By.name("txtCupo_1_1"));
	        Actions seriesOfActionsCupo = builderCupo.moveToElement(nameCupo).click().sendKeys(nameCupo, "15");
	        seriesOfActionsCupo.perform();
	        Actions builderSecc= new Actions(driver);
	        WebElement nameSecc = driver.findElement(By.id("txtSeccion_1_1"));
	        Actions seriesOfActionsSecc = builderSecc.moveToElement(nameSecc).click().sendKeys(nameSecc, "A");
	        seriesOfActionsSecc.perform();
	        
			ReadUrlFile.Wait(5000);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}
