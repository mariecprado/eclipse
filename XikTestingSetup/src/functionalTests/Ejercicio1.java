package functionalTests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.WebElement;

//import org.openqa.selenium.support.ui.Select;
/*
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio1 extends Browsers{

	@Test
	public void f() {
		try {
			
			driver.get("http://compras135.ufm.edu/");
			//driver.findElement(By.id("lst-ib")).sendKeys("xik.gt\n");
			
			ReadUrlFile.Wait(2000);
			driver.findElement(By.cssSelector("button")).click();
			WebElement email = driver.findElement(By.name("identifier"));
			Actions builder = new Actions(driver);
	        Actions seriesOfActions = builder.moveToElement(email).click().sendKeys(email, "xik");
	        seriesOfActions.perform();
	        driver.findElement(By.id("identifierNext")).click();
	        ReadUrlFile.Wait(3000);
	        WebElement passw = driver.findElement(By.cssSelector("input[class='whsOnd zHQkBf']"));
	        Actions builderp = new Actions(driver);
	        Actions seriesOfActionsp = builderp.moveToElement(passw).click().sendKeys(passw, "xik$2015");
	        seriesOfActionsp.perform();
	        driver.findElement(By.id("passwordNext")).click();
	        ReadUrlFile.Wait(5000);
	        driver.findElement(By.cssSelector("img[src='themes/ufm/images/logo-ufm.png']")).click();
	        ReadUrlFile.Wait(5000);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}