package functionalTests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
import auxClasses.WindowHandler;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.WebElement;


//import org.openqa.selenium.support.ui.Select;
/*
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio3 extends Browsers{

	@Test
	public void f() {
		try {
			
			driver.get("http://compras135.ufm.edu/");
			//driver.findElement(By.id("lst-ib")).sendKeys("xik.gt\n");
			
			ReadUrlFile.Wait(2000);
			driver.findElement(By.cssSelector("button")).click();
			WebElement email = driver.findElement(By.name("identifier"));
			Actions builder = new Actions(driver);
	        Actions seriesOfActions = builder.moveToElement(email).click().sendKeys(email, "xik");
	        seriesOfActions.perform();
	        driver.findElement(By.id("identifierNext")).click();
	        ReadUrlFile.Wait(3000);
	        WebElement passw = driver.findElement(By.cssSelector("input[class='whsOnd zHQkBf']"));
	        Actions builderp = new Actions(driver);
	        Actions seriesOfActionsp = builderp.moveToElement(passw).click().sendKeys(passw, "xik$2015");
	        seriesOfActionsp.perform();
	        driver.findElement(By.id("passwordNext")).click();
	        ReadUrlFile.Wait(5000);
	        driver.get("http://compras135.ufm.edu/MiU/index.php");
	        driver.findElement(By.linkText("Hacer LOGIN MiU")).click();
	        ReadUrlFile.waitForLoad(driver);
	        driver.findElement(By.id("divFecla_7")).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[contains(text(),'Soporte a estudiantes')]"))).click();
	        ReadUrlFile.waitForLoad(driver);
	        WebElement usuario = driver.findElement(By.xpath("//input[@name='txtSeleccionUsuario']"));
			Actions builderUser = new Actions(driver);
	        Actions seriesOfActionsUser = builderUser.moveToElement(usuario).click().sendKeys(usuario, "20080633");
	        seriesOfActionsUser.perform();
	        ReadUrlFile.waitForLoad(driver);
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ui-id-4']"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='bttSearch']"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='divFecla_3']"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[contains(text(),'Cursos actuales')]"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='contentTabCursosActuales']/div[5]/table/tbody/tr[23]/td[2]/span"))).click();
	        WindowHandler.next(driver);
	        ReadUrlFile.waitForLoad(driver);
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='contentTabInformacion']/div[2]/div/div[5]/div/table[1]/tbody/tr[1]/td[2]/span"))).click();
	        WebElement Prueba = driver.findElement(By.xpath("//*[@id='divContentModalAdminTarea']/div[2]/div[3]/input"));
			Actions builderPrueba= new Actions(driver);
	        Actions seriesOfActionsPrueba = builderPrueba.moveToElement(Prueba).click().sendKeys(Prueba, "Tarea 1");
	        seriesOfActionsPrueba.perform();//*[@id='btnSaveTarea']
	        WebElement fechaInicio = driver.findElement(By.xpath("//*[@id='txtAdminTareaFechaInicio_dia']"));
			Actions builderFi= new Actions(driver);
	        Actions seriesOfActionsFi = builderFi.moveToElement(fechaInicio).click().sendKeys(fechaInicio, "23");
	        seriesOfActionsFi.perform();
	        WebElement mesInicio = driver.findElement(By.xpath("//*[@id='txtAdminTareaFechaInicio_mes']"));
			Actions builderMes= new Actions(driver);
	        Actions seriesOfActionsMes = builderMes.moveToElement(mesInicio).click().sendKeys(mesInicio, "6");
	        seriesOfActionsMes.perform();
	        WebElement anInicio = driver.findElement(By.xpath("//*[@id='txtAdminTareaFechaInicio_anio']"));
			Actions builderAn= new Actions(driver);
	        Actions seriesOfActionsAn = builderAn.moveToElement(anInicio).click().sendKeys(anInicio, "2017");
	        seriesOfActionsAn.perform();
	        WebElement fechaFin = driver.findElement(By.xpath("//*[@id='txtAdminTareaFechaLimite_dia']"));
			Actions builderFf= new Actions(driver);
	        Actions seriesOfActionsFf = builderFf.moveToElement(fechaFin).click().sendKeys(fechaFin, "25");
	        seriesOfActionsFf.perform();
	        WebElement mesFin = driver.findElement(By.xpath("//*[@id='txtAdminTareaFechaLimite_mes']"));
			Actions builderFin= new Actions(driver);
	        Actions seriesOfActionsFin = builderFin.moveToElement(mesFin).click().sendKeys(mesFin, "10");
	        seriesOfActionsFin.perform();
	        WebElement anFin = driver.findElement(By.xpath("//*[@id='txtAdminTareaFechaLimite_anio']"));
			Actions builderAf= new Actions(driver);
	        Actions seriesOfActionsAnf = builderAf.moveToElement(anFin).click().sendKeys(anFin, "2017");
	        seriesOfActionsAnf.perform();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='btnSaveTarea']"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/header/div/div[2]/div[3]/table/tbody/tr[3]/td/div/input"))).click();
	        WebElement usuario2 = driver.findElement(By.xpath("//input[@name='txtSeleccionUsuario']"));
			Actions builderUser2 = new Actions(driver);
	        Actions seriesOfActionsUser2 = builderUser2.moveToElement(usuario2).click().sendKeys(usuario2, "20130700");
	        seriesOfActionsUser2.perform();
	        ReadUrlFile.waitForLoad(driver);
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ui-id-4']"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='bttSearch']"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='divFecla_3']"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[contains(text(),'Cursos actuales')]"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='contentTabCursosActuales']/div[5]/table/tbody/tr[5]/td[2]/span"))).click();
	        WindowHandler.next(driver);
	        ReadUrlFile.waitForLoad(driver);
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/header/div/div[2]/div[3]/table/tbody/tr[3]/td/div/input"))).click();
	        ReadUrlFile.Wait(5000);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}