package visualTests;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

/**
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio3 extends Browsers{
	// Define screen for sikuli.
	Screen screen;
	
	// Initialize 
	@BeforeClass
	public void init() {
		screen = new Screen();
		//Settings.MinSimilarity = 0.95;
	}
	
	@Test
	public void f() {
		try {
			driver.get("http://compras135.ufm.edu/");
			//encontrar el boton ingresar
			Region region = screen.wait("img/images/ingresar.png").highlight();
			region.click();
			region.click();
			
			//Email
			ReadUrlFile.Wait(6000);
			region = screen.wait("img/images/emailLogin.png");
			region.click();
			region.type("xik");
			region = screen.wait("img/images/nextEmail.png");
			region.click();
			
			//Password
			region = screen.wait("img/images/passLoginSc.png");
			region.type("xik$2015");
			region = screen.wait("img/images/nextEmail.png");
			region.click();
			ReadUrlFile.Wait(6000);
			
			driver.get("http://compras135.ufm.edu/MiU");
			ReadUrlFile.Wait(2000);
			region = screen.wait("img/images/hacerLogin.png");
			region.click();
			ReadUrlFile.Wait(4000);
			region = screen.wait("img/images/Recursos.png");
			region = region.wait("img/images/flechita.png");
			region.click();
			region = screen.wait("img/images/soporte.png");
			region.click();
			ReadUrlFile.Wait(2000);
			region = screen.wait("img/images/Usuario.png");
			//region = region.wait("img/images/IntroCarne.png");
			region.click();
			region.click();
			region.type("20080633");
			region = screen.wait("img/images/name.png");
			region.click();
			region = screen.wait("img/images/Emular.png");
			region.click();
			ReadUrlFile.Wait(2000);
			region = screen.wait("img/images/cursos.png");
			region = region.wait("img/images/flechitacursos.png");
			region.click();
			ReadUrlFile.Wait(2000);
			region = screen.wait("img/images/CursosActuales.png");
			region.click();
			ReadUrlFile.Wait(4000);
			//region = screen.wait("img/images/.png");
			//region = screen.wait("img/images/elegirCurso2.png");
			region = screen.wait("img/images/cursoelegido.png");
			region.click();
			region.click();
			ReadUrlFile.Wait(6000);
			region = screen.wait("img/images/parte.png");
			ReadUrlFile.Wait(2000);
			region = screen.wait("img/images/Anuncio.png");
			region = region.wait("img/images/Agregar.png");
			region.click();
			ReadUrlFile.Wait(2000);
			region = screen.wait("img/images/Asunto.png");
			region.click();
			region.type("Prueba");
			region = screen.wait("img/images/Mensaje.png");
			region.click();
			region.type("Esto es una prueba");
			region = screen.wait("img/images/Guardar.png");
			region.click();
			
			/*region.click();
			region = screen.wait("img/images/terminar.png");
			region.click();
			region.click();*/
			
			ReadUrlFile.Wait(4000);
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}	
